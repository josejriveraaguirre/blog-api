package com.delta.blog.blog.services;

import java.util.List;

import com.delta.blog.blog.models.Post;

public interface PostService {

    List<Post> findAll();

    //Method to save an object of our model - "Post"
    Post save(Post post);

    //method to find an object by id

    Post findById(Long id);

    // method to delete an object from the database
void delete(Long id);
    
}
