package com.delta.blog.blog.controllers;

import java.util.List;

import com.delta.blog.blog.models.Post;
import com.delta.blog.blog.services.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*") //localhost 8080 can be requested from any other localhost including Angular's (4200)
@RestController
@RequestMapping("/api/v1")
public class PostController {

    @Autowired
    PostService pService;

    // get method to GET data from our database -> table names "posts"

    @GetMapping("/posts")
    public ResponseEntity<List<Post>> get() {
        List<Post> posts = pService.findAll();

        return new ResponseEntity<List<Post>>(posts, HttpStatus.OK);
    }
    // post method to post new data to our database
    @PostMapping("/posts")
    public ResponseEntity<Post> save(@RequestBody Post post) {
        Post newPost = pService.save(post);

        return new ResponseEntity<Post>(newPost, HttpStatus.OK);
    }

    // get method to get the data of an individual post object based on the id property

    @GetMapping("/posts/{id}")
    public ResponseEntity<Post> getPost(@PathVariable("id") Long id) {
        Post viewPost = pService.findById(id);
        return new ResponseEntity<Post>(viewPost, HttpStatus.OK);
    }

    // delete method to delete an individual post object based on it's id

    @DeleteMapping("/posts/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        pService.delete(id);
        return new ResponseEntity<String>("Post is deleted successfully!", HttpStatus.OK);
    }
}
